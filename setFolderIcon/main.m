//
//  main.m
//  setFolderIcon
//
//  Created by Frantisek Erben on 27.01.17.
//  Copyright © 2017 Frantisek Erben (erben.fr@gmail.com). All rights reserved.
//

#import <AppKit/AppKit.h>


int main(int argc, const char * argv[]) {
   @autoreleasepool {

      NSArray  *args    = [[NSProcessInfo processInfo] arguments];
      NSString *helpStr = (@"Usage: setFolderIcon [file.icns|png|jpg|tiff] [folder]\n© 2017 František Erben (erben.fr@gmail.com)");
      if (args.count>1 && args.count<4) {
         NSURL *icnsFile     = [[NSURL alloc] initFileURLWithPath:args[1]];
         NSURL *targetFolder = [[NSURL alloc] initFileURLWithPath:args[2]];
         NSImage *icnsImage  = [[NSImage alloc] initWithContentsOfURL:icnsFile];
         NSWorkspace *ws = [NSWorkspace sharedWorkspace];
         BOOL res = [ws setIcon:icnsImage forFile:[targetFolder path] options: NSExcludeQuickDrawElementsIconCreationOption];
         if (res) {
            printf("Icon was set successfully!\n");
            exit(0);
         }
         else {
            printf("Something was wrong. Please check parameter's order\n%s", [helpStr UTF8String]);
            exit(1);
         }
      }
      else {
         printf("%s", [helpStr UTF8String]);
         exit(0);
      }
   }
}
